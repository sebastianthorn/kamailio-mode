# kamailio-mode
This i a mode that adds syntax-highlightning to kamailio-files.
This should be considerd as a work in progress, please help if you can.

Add this to your `.emacs` file:
```
;; my own kamailio-mode
(load-file "~/.emacs.d/kamailio.el")
```

Add the file kamailio.el to your `.emacs.d` folder.

If you add `# -*- mode: kamailio -*-` at the top of your kamailio-files to auto-load the mode.
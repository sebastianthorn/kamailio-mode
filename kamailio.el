;; Kamailio keywords
(setq kamailio-keywords
	  `(
        ("#[a-zA-Z0-9\s\t\n#\"].*" . font-lock-comment-face) ;; need to NOT catch #!... Might be able to use [^!]
		
		;; Strings
        ;;("\s".*\s"" . font-lock-string-name-face) ;; not getting this to work
		
		;; Function-names
        ( ,(regexp-opt '("add_path" "add_rport" "add_rr_param" "add_tcp_alias"
						 "alias" "allow_source_address" "append_branch" "xlog"
						 "append_hf" "auth_challenge" "auth_check" "children"
						 "auto_aliases" "avpflags" "branch_route" "cfg_reset"
						 "cfg_select" "check_route_param" "clear_branches"
						 "consume_credentials" "debug" "disable_tcp" "memlog"
						 "error" "exit" "failure_route" "fix_nated_register"
						 "force_rport" "force_send_socket" "force_tcp_alias"
						 "fork" "forward" "forward_sctp" "forward_tcp" "uri"
						 "forward_tls" "forward_udp" "from_uri" "enable_tls"
						 "handle_ruri_alias" "has_totag" "include_file"
						 "is_first_hop" "is_method" "is_request" "isavpflagset"
						 "isbflagset" "isflagset" "listen" "loadmodule" "log"
						 "log_facility" "log_prefix" "log_stderror" "lookup"
						 "loose_route" "memdbg" "userphone" "xdbg" "setuserpass"
						 "modparam" "mpath" "myself" "mf_process_maxfwd_header"
						 "nat_uac_test" "onreply_route" "onsend_route"
						 "pike_check_req" "port" "prefix" "record_route"
						 "remove_branch" "remove_hf" "udp_mtu_try_proto"
						 "resetavpflag" "resetflag" "return" "revert_uri"
						 "rewritehost" "rewritehostport" "rewritehostporttrans"
						 "rewriteport" "rewriteuri" "rewriteuser" "udp_mtu"
						 "rewriteuserpass" "route" "sanity_check" "save" "send"
						 "send_reply" "send_tcp" "set_advertised_address"
						 "set_advertised_port" "set_contact_alias" "setavpflag"
						 "setbflag" "setflag" "seth" "sethost" "sethostport"
						 "sethostporttrans" "sethp" "sethpt" "setp" "setport"
						 "setu" "setup" "seturi" "setuser" "request_route"
						 "sl_reply_error" "sl_send_reply" "src_ip" "status"
						 "strip" "strip_tail" "t_check_status" "t_check_trans"
						 "t_is_branch_route" "t_is_canceled" "t_is_set"
						 "t_newtran" "t_on_branch" "t_on_failure" "t_on_reply"
						 "t_precheck_trans" "t_relay" "tcp_connection_lifetime"
						 "import_file"
						 ) 'word) . font-lock-function-name-face)

		;; Regular Psudo-variables
		( ,(regexp-opt '("$ai" "$adu" "$ar" "$au" "$ad" "$aU" "$Au" "$branch"
						 "$br" "$bR" "$bf" "$bF" "$bs" "$ci" "$cl" "$cs" "$ct"
						 "$cT" "$dd" "$di" "$dip" "$dir" "$dic" "$dp" "$dP"
						 "$ds" "$du" "$err.class" "$err.level" "$err.info"
						 "$err.rcode" "$err.rreason" "$fd" "$fn" "$ft" "$fu"
						 "$fU" "$mb" "$mf" "$mF" "$mi" "$ml" "$mt" "$od" "$op"
						 "$oP" "$ou" "$oU" "$pd" "$pn" "$pp" "$pr" "$proto"
						 "$pU" "$pu" "$rd" "$rb" "$rc" "$retcode" "$rc" "$re"
						 "$rm" "$rmid" "$rp" "$rP" "$rr" "$rs" "$rt" "$ru" "$rU"
						 "$rz" "$Ri" "$Rp" "$sf" "$sF" "$si" "$sp" "$stat" "$fs"
						 "$td" "$tn" "$tt" "$tu" "$tU" "$Tb" "$Tf" "$TF" "$Ts"
						 "$TS" "$ua "
						 ) 'word) . font-lock-constant-name-face)
		
		;; Special Psudo-variables
		( ,(regexp-opt '("$xavp" "$avp" "$var" "$hdr" "$shv" "$time" "$sel"
						 "$snd" "$sndfrom" "$sndto" "$BM_time_diff" "$dlg"
						 "$dlg_ctx" "$dlg_var" "$sht" "$shtex" "$shtcn" "$shtcv"
						 "$shtinc" "$shtdec" "$mct" "$mcinc" "$mcdec" "$xml"
						 "$T_branch_idx" "$T_reply_code" "$T_req" "$T_rpl"
						 "$T_inv" "$T" "$uac_req" "$rr_count" "$rr_top_count"
						 "$mqk" "$mqv" "$TV" "$nh" "$gip" "$hu" "$msrp" "$C"
						 ) 'word) . font-lock-constant-name-face)
		
		;; Operators
        ( ,(regexp-opt '("#!define" "#!ifdef" "#!else" "#!endif" "if" "else" ";"
						 "$null" "&&" "||" "=~" "!=" "switch" "case" "=>" "=<"
						 "<" ">"
						 ) 'word) . font-lock-keyword-face)
        )
	  )

;; Kamailio syntax table
(defvar kamailio-syntax-table nil "Syntax table for `kamailio-mode'.")
(setq kamailio-syntax-table
      (let ((synTable (make-syntax-table)))
		
		;; bash style comment: "# ...
        ;;(modify-syntax-entry ?# "< b" synTable)
        ;;(modify-syntax-entry ?\n "> b" synTable)

		;; java style comment: "/* ... */"
		(modify-syntax-entry ?\/ ". 14" synTable)
		(modify-syntax-entry ?* ". 23" synTable)
		
        synTable)
	  )

(define-derived-mode kamailio-mode fundamental-mode ;; fundameltal-mode seems to screw things up, text-mode seems to work better
  "kamailio-mode is a major mode for editing kamailio-files."
  :syntax-table kamailio-syntax-table
  (setq font-lock-defaults '(kamailio-keywords))
  (setq mode-name "kamailio")
  )
